import  App  from '../../app';
const chai = require("chai");
const chaiHttp = require("chai-http");
import { CONFIG } from '../config';
import "reflect-metadata";
import { BookController } from "../controller/bookController";
import { BookService } from "../services/bookService"
chai.use(chaiHttp);
const expect = chai.expect;
import * as express from 'express';
const httpMocks = require('node-mocks-http');


// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
// import 'mocha';
const app = new App(CONFIG.PORT , CONFIG.DB_STRING);
describe('Hello function', () => {
  let  controller;
  beforeEach(() => {
    controller = new BookController(new BookService());
  });

    // it('should get back all user', () => {
    //   expect(controller.getUsers()).to.deep.equal(
    //     [{
    //       "title": 'lorem@ipsum.com',
    //       "category": 'Lorem',
    //       "description": 'description',
    //     }]
    //   );
    // });


    const request = httpMocks.createRequest({
      method: 'GET',
      url: '/api/book'
  });

  const response = httpMocks.createResponse();

    it('should give back the right user', () => {
      console.log('123: ', expect(controller.getUser(request,response)))
      expect(controller.getUser(request,response)).to.deep.equal("123");
    });

    // expect(controller.list().to.deep.equal(
    //   [{
    //     email: 'lorem@ipsum.com',
    //     name: 'Lorem'
    //   }, {
    //       email: 'doloe@sit.com',
    //       name: 'Dolor'
    //     }]
    // );
    // chai
    //   .request(app)
    //   .post("/getToken")
    //   .send({ "username": "admin", "password": 123 })
    //   .end((err, res) => {
    //     expect(res).to.have.status(200);
    //     expect(res.body.status).to.equals("success");
    //     done();
    //   });
})
