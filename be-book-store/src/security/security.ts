import to from "await-to-js";
import User from "../models/IUserModel";
import { authenticate, createdToken } from "../middlewares/authenticate"
export async function security(req, res, next){
    if(req.originalUrl === '/login'){
        const [err, data] = await to(User.findOne({email: req.body.email, password: req.body.password}).exec());
        if (err) {
            throw err;
        }
        if(data){
            let token = createdToken();
            res.header("x-auth-token", token).send({
                result : 201,
                data : data,
                token: token
            });
        }else{
            res.send({
                result : 200,
                data :  'user not found'
            });
        }

    }else if(req.originalUrl === '/register'){
        let { email, password, role } = req.body;
        let user = new User({ email, password, role });
        const [err, data] = await to(user.save());
        if(data){
            let token = createdToken();
            res.header("x-auth-token", token).send({
                result : 201,
                data : data,
                token: token
            });
        }else{
            res.send({
                result : 400,
                data :  err
            });
        }
    }else if(req.originalUrl === '/logout'){
        res.header("x-auth-token", null).send({
            result : 201,
            data : {}
        });
    }else{
        authenticate(req, res, next)
    }

}