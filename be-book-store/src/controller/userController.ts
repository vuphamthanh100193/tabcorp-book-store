import * as express from 'express';
import { injectable, inject  } from 'inversify';
import { interfaces, controller, httpGet, httpPost, request, response, httpPut, httpDelete } from "inversify-express-utils";
import { UserService } from "../services/userService";
import { handleError } from "../middlewares/error";
import { senToClient } from "../middlewares/mapData";
import winston from "../config/winston";

@controller("/user")
export  class UserController  implements interfaces.Controller {

    constructor( @inject("UserService") private userService: UserService ) {
    }

    @httpPost("/login")
    public async getUser(@request() req: express.Request, @response() res: express.Response)  {
        try {
          let data = await this.userService.findByEmailAndPassword(req.body);
            senToClient(201, data, res);
        } catch (err) {
            winston.error(` [${__filename}] get list book error: ${err}`);
            handleError({statusCode: 400, message: err}, res);
        }
    }

    @httpPost("/api/register")
    private async create(@request() req: express.Request, @response() res: express.Response) {
        try {
            await this.userService.addUser(req.body);
            senToClient(201 ,req.body, res);
        } catch (err) {
            winston.error(` [${__filename}] create book error: ${err}`);
            handleError({statusCode: 400, message: err}, res);
        }
    }
    // @httpPut("/")
    // private async update(@request() req: express.Request, @response() res: express.Response) {
    //     try {
    //         await this.bookService.update(req.body);
    //         senToClient(201 ,req.body, res);
    //     } catch (err) {
    //         winston.error(` [${__filename}] update book error: ${err}`);
    //         handleError({statusCode: 400, message: err}, res);
    //     }
    // }
    // @httpDelete("/")
    // private async delete(@request() req: express.Request, @response() res: express.Response) {
    //     try {
    //         await this.bookService.delete(req.body);
    //         senToClient(201 ,req.body._id, res);
    //     } catch (err) {
    //         winston.error(` [${__filename}] delete book error: ${err}`);
    //         handleError({statusCode: 400, message: err}, res);
    //     }
    // }
}
export default UserController;