export interface IUser {
    email: String,
    password: String,
    role: String
}
