export interface IBook {
    title: String,
    image: String,
    category: String,
    quantity: number,
    price: String,
    description: String
}
