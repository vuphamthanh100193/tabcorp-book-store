export interface IBookServices {
    findAll(),
    addBook(data),
    update(data),
    delete(_id),
}
