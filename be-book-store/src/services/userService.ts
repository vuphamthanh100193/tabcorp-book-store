import User from "../models/IUserModel";
import to from "await-to-js";
import { injectable, inject } from "inversify";
import { IUserServices } from '../interfaces/IUserService';
import { IUser } from '../interfaces/IUser';

@injectable()
class UserService implements IUserServices {
    public async findByEmailAndPassword(body) {
        const [err, data] = await to(User.findOne({email: body.email, password: body.password}).exec());
        if (err) {
            throw err;
        }
        return data;
    }

    public async addUser(body) {
        let { email, password, role } = body;
        let user = new User({ email, password, role });
        const [err, data] = await to(user.save());
        if (err) {
            throw err;
        }
        return data;
    }

  

   
}

export { UserService };