import * as mongoose from 'mongoose';
// import uuidv1 from 'uuid/v1';



import { IUser } from '../interfaces/IUser';

interface IUserModel extends IUser, mongoose.Document{}

var userSchema = new mongoose.Schema({

  email: {
    type: String,
    required: true,
    maxlength:30,
    unique: true,
  },
  password: {
    type:String,
    required: true,
  },
  role: {
    type: String,
    default: 'user',
    required: true
  }
})

var User = mongoose.model<IUserModel>("User", userSchema);


export default User;