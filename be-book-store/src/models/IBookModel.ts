import * as mongoose from 'mongoose';
// import uuidv1 from 'uuid/v1';



import { IBook } from '../interfaces/IBook';

interface IBookModel extends IBook, mongoose.Document{}

var bookSchema = new mongoose.Schema({

  title: {
    type: String,
    required: true,
    maxlength:30,
    unique: true,
  },
  image: {
    type: String,
    required: true,
  },
  category: {
    type:String,
    enum: ['drama', 'comedy', 'sport'],
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
    maxlength:30,
  },
  price: {
    type: Number,
    required: true,
    maxlength:30,
  },
  description: {
    type: String,
  }
})

var Book = mongoose.model<IBookModel>("Book", bookSchema);


export default Book;