import appRoot from 'app-root-path';
let winston = require('winston');
let winstonDaily = require('winston-daily-rotate-file');

// define the custom settings for each transport (file, console)
let options = {
    file: {
        level: 'info',
        filename: `${appRoot}/logs/app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
  
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};

let transport = new (winston.transports.DailyRotateFile)({
    filename: `${appRoot}/logs/%DATE%-error.log`,
    datePattern: 'YYYY-MM-DD',
    maxSize: '20m',
    maxFiles: '14d',
  });
 
// instantiate a new Winston Logger with the settings defined above
let logger = new winston.createLogger({
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
        }),
        winston.format.prettyPrint(),
        winston.format.printf((info) => {
            return `${info.level}  ${info.timestamp}  ${info.message} `;
        }),
    ),
    // transports: [
    //     new winston.transports.File(options.file),
    //     new winston.transports.Console(options.console)
    // ],
    transports: [
        transport,
        new winston.transports.Console(options.console)
      ],
    exitOnError: false
});

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
    write: function(message, encoding) {
        // use the 'info' log level so the output will be picked up by both transports (file and console)
        logger.info(message);
    },
};

export default logger;