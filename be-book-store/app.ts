import express  from 'express';
import {Container} from 'inversify';
import {InversifyExpressServer} from 'inversify-express-utils';
import "./src/controller/bookController";
import * as bodyParser from 'body-parser';
import { getDatabaseClient } from './src/db/db_client';
import cors from 'cors';
import "reflect-metadata";
import './src/controller/userController';
import {BookService} from "./src/services/bookService";
import {UserService} from "./src/services/userService";

import { authenticate } from "./src/middlewares/authenticate"
import {security} from "./src/security/security";

class App {
    public app: express.Application;
    public port: Number;
    public db_string: string;
    

    constructor(port: Number, db_string: string) {
        this.app = express();
        this.port = port;
        this.db_string = db_string;
        this.connectToDB();
        this.listen();
    }

    private async connectToDB() {
        await getDatabaseClient(this.db_string);

    }

    public listen() {
        
    // set up container
        let container = new Container();
    // set up bindings
        container.bind<BookService>('BookService').to(BookService);
        container.bind<UserService>('UserService').to(UserService);
        
    // create server
        let server = new InversifyExpressServer(container);
        server.setConfig((app) => {
            // add body parser
            app.use(bodyParser.urlencoded({
                extended: true
            }));
            app.use(bodyParser.json());
            app.use(cors());
            app.use('/', (req, res, next)        => security(req, res, next));
            // app.use('/api', (req, res, next)        => authenticate(req, res, next));
        });
        let app = server.build();
       // this.getToken(app);
        app.listen(this.port, () => {
            console.info(`App is running on port ${this.port}`)           
        })
      
    }
}

export default App;
