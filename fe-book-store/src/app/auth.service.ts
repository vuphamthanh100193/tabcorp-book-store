import {Injectable, Injector} from '@angular/core';

import {ApiService} from './api.service';
import {BookResponse} from './book-response';
import {Router} from '@angular/router';

import { Observable, throwError } from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class AuthService {

  constructor(private apiService: ApiService, private injector: Injector) { }
  isLoggedIn = false;
  currentUser: any;
  moduleRouter: any;

  setRoute(route) {
    this.moduleRouter = route;
  }

   public login(email, password, url): Observable<any> {
    const body = new HttpParams()
      .set('email', email)
      .set('password', password)
    return this.apiService.postRequest(body, url);
  }

  logout(url): Observable<any> {
    sessionStorage.clear();
    localStorage.clear();
    const body = new HttpParams()
    this.apiService.postRequest(body, url).subscribe((res: BookResponse) => {
      if (res && res.result === 0) {
        this.isLoggedIn = false;
        this.currentUser = null;
      }
      this.injector.get(Router).navigate(['/login']);
    });
    return;
  }
}
