import {Injectable, Injector} from '@angular/core';

import {AuthService} from './auth.service';
import {HttpParams} from '@angular/common/http';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';

@Injectable()
export class StartupService {
   authService: AuthService;

  constructor(private injector: Injector, private apiService: ApiService) {
  }

  // This is the method you want to call at bootstrap
  // Important: It should return a Promise
  load(): Observable<any> {
    this.authService = this.injector.get(AuthService);

    const body = new HttpParams()
      .set('email', 'admin@gmail.com')
      .set('password', '123456')
    return this.apiService.postRequest(body, '/login');
  }

}
