import {Injectable, Injector} from '@angular/core';
// import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { BookRequest } from './book-request';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ApiService {//
  private URL = 'http://localhost:5000';

  constructor(private http: HttpClient) {
  }
  public getRequest(params: HttpParams, url: string): Observable<any> {
    return this.http.get(`${this.URL}${url}`, {params,
      headers : new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('access-token', localStorage.getItem('token') || '')} )
      .pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }
  public postRequest(params: HttpParams, url: string): Observable<any> {
    return this.http.post(`${this.URL}${url}`, params, {headers : new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('access-token', localStorage.getItem('token') || '') } )
      .pipe(
        map(this.extractData),
        catchError(this.handleError)
      );
  }


  private extractData(res: Response) {
    return res || {};
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
