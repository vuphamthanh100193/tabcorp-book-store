import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  currentUser = '';
  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUser;
  }
  logout(){
    this.authService.logout('/logout')
      .subscribe(
        (response: any) => {
          if (response && response.result === 201) {
            this.authService.isLoggedIn = false;
            this.authService.currentUser = response.data.role;
            this.authService.setRoute(response.data.pages);
            this.router.navigate(['/list-book']);
          }
        },
        error => {
        });
  }
}
