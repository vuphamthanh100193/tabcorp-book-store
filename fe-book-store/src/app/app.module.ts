import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {LoginComponent} from './user/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { NewBookComponent } from './book/new-book/new-book.component';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import { ListBookComponent } from './book/list-book/list-book.component';
import {MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import { HttpClientModule } from '@angular/common/http';
import {AlertService} from './alert.service';
import {AuthService} from './auth.service';
import {ApiService} from './api.service';
import {BookService} from './services/book.service';
import {BookDetailComponent} from './book/book-detaik/book-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NewBookComponent,
    ListBookComponent,
    BookDetailComponent
  ],
  imports: [
    HttpClientModule,
    MatSortModule,
    MatPaginatorModule,
    MatTableModule,
    MatListModule,
    MatInputModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatCardModule
  ],
  providers: [AlertService, AuthService, ApiService, BookService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
