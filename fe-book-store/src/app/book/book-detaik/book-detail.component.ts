import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatTableDataSource} from "@angular/material/table";
import {BookService} from "../../services/book.service";

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {

  private id: string;
  private sub: any;

  title = '';
  image = '';
  category = '';
  quantity  = 0;
  price = 0;
  description = '';
  addBookForm: FormGroup;
  submitted = false;
  categories = ['comedy', 'drama', 'sport'];

  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private bookService: BookService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {

      this.id = params.id;
      this.bookService.getById(`/api/book?id=${this.id}`)
        .subscribe(
          (response: any) => {
            if (response && response.result === 201) {
              this.title = response.data.title || '';
              this.image = response.data.image || '';
              this.category = response.data.category || '';
              this.quantity = response.data.quantity || 0;
              this.price = response.data.price || 0;
              this.description = response.data.description;
            }
          },
          error => {
          });

    });
    this.addBookForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.email]],
      image: [],
      category: [],
      quantity: [],
      price: [],
      description: [],
    });

  }

  ngOnDestroy() {

    this.sub.unsubscribe();
  }

}
