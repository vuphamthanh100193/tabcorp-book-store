import { Component, OnInit, ViewChild } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { BookService} from '../../services/book.service';

@Component({
  selector: 'app-list-book',
  templateUrl: './list-book.component.html',
  styleUrls: ['./list-book.component.css']
})
export class ListBookComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  listBookForm: FormGroup;
  books = []
  displayedColumns: string[] = ['title', 'image', 'quantity', 'category'];
  dataSource = new MatTableDataSource(this.books)

  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.bookService.getList('/api/book/list')
      .subscribe(
        (response: any) => {
          if (response && response.result === 201) {
            console.log(response);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.books = response.data || [];
            this.dataSource = new MatTableDataSource(this.books);
          }
        },
        error => {
        });
  }
}
