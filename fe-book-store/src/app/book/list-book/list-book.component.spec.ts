import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {FormBuilder, FormsModule, ReactiveFormsModule, Validators,} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {BrowserModule} from '@angular/platform-browser';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { ListBookComponent } from './list-book.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MatListModule} from '@angular/material/list';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {ApiService} from '../../api.service';
import {BookService} from '../../services/book.service';
import {of} from 'rxjs';
import {MatSortModule} from '@angular/material/sort';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('ListBookComponent', () => {
  let component: ListBookComponent;
  let fixture: ComponentFixture<ListBookComponent>;
  const formBuilder: FormBuilder = new FormBuilder();
  let bookService: BookService;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(async(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    const spy = jasmine.createSpyObj('ApiService', ['getRequest']);
    TestBed.configureTestingModule({
        declarations: [ ListBookComponent ],
        imports: [BrowserAnimationsModule, MatSortModule, MatListModule, MatTableModule,
          MatCardModule, FormsModule, BrowserModule],
        providers: [
          BookService, { provide: ApiService, useValue: spy },
          MatTableDataSource
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBookComponent);
    component = fixture.componentInstance;
    bookService = TestBed.get(BookService);
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call ngOnInit and call  bookService ', () => {
    const response = {
      result: 201,
      data: [
        {title: 1, image: 'Hydrogen', quantity: 1.0079, category: 'H'},
        {title: 2, image: 'Helium', quantity: 4.0026, category: 'He'},
        {title: 3, image: 'Lithium', quantity: 6.941, category: 'Li'},
      ]
    };
    spyOn(bookService, 'getList').and.returnValue(of(response));
    spyOn(component, 'ngOnInit').and.callThrough();
    component.ngOnInit();
    expect(bookService.getList).toHaveBeenCalled();
    expect(component.books).toEqual(response.data);
    // expect(component.dataSource).toBe(new MatTableDataSource(response.data));
  });
});
