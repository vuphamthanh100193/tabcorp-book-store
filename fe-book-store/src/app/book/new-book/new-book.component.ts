import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";
import {BookService} from "../../services/book.service";
import {AuthService} from "../../auth.service";

@Component({
  selector: 'app-book-new-book',
  templateUrl: './new-book.component.html',
  styleUrls: ['./new-book.component.css']
})
export class NewBookComponent implements OnInit {
  title = '';
  image = '';
  category = '';
  quantity  = 0;
  price = 0;
  description = '';
  addBookForm: FormGroup;
  submitted = false;
  categories = ['comedy', 'drama', 'sport'];

  constructor(private router: Router, private formBuilder: FormBuilder
              , private bookService: BookService, private authService: AuthService) { }

  ngOnInit() {
    this.addBookForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      image: [],
      category: [],
      quantity: [],
      price: [],
      description: [],
    });
  }
  addBook() {
      this.submitted = true;
      // stop the process here if form is invalid
      if (this.addBookForm.invalid) {
        return;
      }

      this.bookService.addBook(this.title, this.image, this.category, this.quantity, this.price,
        this.description, '/api/book').pipe()
        .subscribe(
          (response: any) => {
            if (response && response.result === 201) {
              this.router.navigate(['/list-book']);
            } else {
              alert('Đã xãy ra lỗi, vui lòng thử lại');
            }
          },
          error => {
          });

  }
}
