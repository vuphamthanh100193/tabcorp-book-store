import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './user/login/login.component';
import {RegisterComponent} from './user/register/register.component';
import {NewBookComponent} from './book/new-book/new-book.component';
import {ListBookComponent} from './book/list-book/list-book.component';
import {AuthGuard} from './auth.guard';
import {BookDetailComponent} from "./book/book-detaik/book-detail.component";
const routes: Routes = [
  { path: '',        component: LoginComponent },
  { path: 'login',        component: LoginComponent },
  { path: 'register',        component: RegisterComponent },
  { path: 'new-book',    canActivate: [AuthGuard],    component: NewBookComponent },
  { path: 'list-book',   canActivate: [AuthGuard],     component: ListBookComponent, runGuardsAndResolvers: 'always' },
  { path: 'book-detail/:id',    canActivate: [AuthGuard],    component: BookDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule],
  providers: [
    AuthGuard,
  ]
})
export class AppRoutingModule { }
