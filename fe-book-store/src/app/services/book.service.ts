import {Injectable, Injector} from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {ApiService} from '../api.service';

@Injectable()
export class BookService {

  constructor(private apiService: ApiService) { }
  isLoggedIn = false;
  currentUser: any;
  moduleRouter: any;

  public getList(url): Observable<any> {
    const body = new HttpParams();
    return this.apiService.getRequest(body, url);
  }

  getById(url): Observable<any> {
    const body = new HttpParams();
    return this.apiService.getRequest(body, url);
  }

  addBook(title, image, category, quantity, price, description, url): Observable<any> {
    const body = new HttpParams()
      .set('title', title)
      .set('image', image)
      .set('category', category)
      .set('quantity', quantity)
      .set('price', price)
      .set('description', description)
    return this.apiService.postRequest(body, url);
  }
}
