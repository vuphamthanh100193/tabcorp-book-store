import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import {of} from 'rxjs';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {MatCardModule} from '@angular/material';
import {FormBuilder, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {AuthService} from '../../auth.service';
import {ApiService} from '../../api.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  const formBuilder: FormBuilder = new FormBuilder();
  let authService: AuthService;
  let httpClientSpy: { post: jasmine.Spy };

  beforeEach(async(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    const spy = jasmine.createSpyObj('ApiService', ['postRequest']);
    TestBed.configureTestingModule({
      declarations: [ RegisterComponent ],
      imports: [MatCardModule, FormsModule, BrowserModule, ReactiveFormsModule],
      providers: [
        { provide: FormBuilder, useValue: formBuilder  },
        { provide: ActivatedRoute, useValue: {returnUrl : '/'} },
        { provide: Router, useValue: {} },
        AuthService, { provide: ApiService, useValue: spy },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    authService = TestBed.get(AuthService);
    // pass in the form dynamically
    component.registerForm = formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call ngOnInit return  returnUrl equal /list-book', () => {
    spyOn(component, 'ngOnInit').and.callThrough();
    component.ngOnInit();
    expect(component.returnUrl).toEqual('/list-book');
  });

  it('should call register', () => {
    spyOn(authService, 'login');
    component.registerForm.controls.email.setValue('admin@gmail.com');
    component.registerForm.controls.password.setValue('123123');
    component.registerForm.controls.confirmPassword.setValue('123123');
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    expect(authService.login).toHaveBeenCalled();
  });

  it('should call register return success', () => {
    const response = {
      result: 201,
      data: {
        role: 'admin',
        token: 'sdqwexcoash'
      }
    };
    component.registerForm.controls.email.setValue('admin@gmail.com');
    component.registerForm.controls.password.setValue('123123');
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    spyOn(authService, 'login').and.returnValue(of(response));
    expect(authService.isLoggedIn).toEqual(false);
  });
});
