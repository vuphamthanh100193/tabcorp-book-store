import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MustMatch} from '../../../validator/must-match';
import 'src/app/models/responer';
import {switchMap} from "rxjs/operators";
import {interval} from "rxjs";
import {AuthService} from "../../auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService} from "../../alert.service";

@Component({
  selector: 'app-user-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email = '';
  password = '';
  confirmPassword = '';
  registerForm: FormGroup;
  submitted = false;
  returnUrl: string;

  constructor(private formBuilder: FormBuilder,
              private router: Router, private route: ActivatedRoute, private authService: AuthService) { }

  ngOnInit() {
    this.returnUrl =  '/list-book';
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  register() {
    this.submitted = true;
    // stop the process here if form is invalid
    console.log(this.registerForm.value);
    if (this.registerForm.invalid) {
      return;
    }

    this.authService.login(this.email, this.password, '/register')
      .subscribe(
        (response: any) => {
          if (response && response.result === 201) {
            this.authService.isLoggedIn = true;
            this.authService.currentUser = response.data.role;
            this.authService.setRoute(response.data.pages);
            localStorage.setItem('isLoggedIn', 'true');
            this.router.navigate([this.returnUrl]);
          } else {
            alert('Thông tin đăng nhập không chính xác');
          }
        },
        error => {
          // this.alertService.error(error);
        });
  }
}
