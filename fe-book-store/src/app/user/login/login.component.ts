import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 'src/app/models/responer';
import {ApiService} from '../../api.service';
import {AuthService} from '../../auth.service';
import {AlertService} from '../../alert.service';
import {ActivatedRoute, Router, Routes} from '@angular/router';
// import {AuthGuard} from '../../auth.guard';
import {RegisterComponent} from '../register/register.component';
import {AppComponent} from '../../app.component';
import { switchMap } from 'rxjs/operators';
import {interval} from "rxjs";

@Component({
  selector: 'app-user-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email  = '';
  password = '';
  @Input()  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,
              private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.returnUrl =  '/list-book';
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  login() {
      this.submitted = true;
      // stop the process here if form is invalid
      if (this.loginForm.invalid) {
        return;
      }

      this.authService.login(this.email, this.password, '/login')
        .subscribe(
          (response: any) => {
            if (response && response.result === 201) {
              this.authService.isLoggedIn = true;
              this.authService.currentUser = response.data.role;
              this.authService.setRoute(response.data.pages);
              localStorage.setItem('isLoggedIn', 'true');
              localStorage.setItem('token', response.token);
              // const  startRoute: Routes = this.router.config.slice(0, 2);
              // const  authRoute: Routes = response.data.pages;
              // startRoute.push(
              //   {path: '/', component: RegisterComponent,  children: authRoute}
              // );
              // startRoute.push({path: '**', redirectTo: ''});
              // console.log('c route: ', startRoute);
              // this.router.resetConfig(startRoute);
              this.router.navigate([this.returnUrl]);
            } else {
              // alert('Thông tin đăng nhập không chính xác');
            }
          },
          error => {
            // this.alertService.error(error);
          });

    // this.service.login(this.email, this.password).subscribe((res: Responer) => {
    //   // console.log(JSON.stringify(res))
    //   alert(`Welcom   ${this.email}!!`);
    // });
  }

}
