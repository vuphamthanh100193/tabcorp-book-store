import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {FormBuilder, FormsModule, ReactiveFormsModule, Validators,} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {BrowserModule} from '@angular/platform-browser';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { of } from 'rxjs';
import {AuthService} from '../../auth.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ApiService} from '../../api.service';


describe('Isolated test LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  const formBuilder: FormBuilder = new FormBuilder();
  let authService: AuthService;
  let httpClientSpy: { post: jasmine.Spy };
  const router1 = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    const spy = jasmine.createSpyObj('ApiService', ['postRequest']);
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [MatCardModule, FormsModule, BrowserModule, ReactiveFormsModule],
      providers: [
        { provide: FormBuilder, useValue: formBuilder  },
        { provide: ActivatedRoute, useValue: {returnUrl : '/'} },
        { provide: Router, useValue: router1 },
        AuthService, { provide: ApiService, useValue: spy },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    authService = TestBed.get(AuthService);
    component = fixture.componentInstance;
    // pass in the form dynamically
    component.loginForm = formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnInit return  returnUrl equal /list-book', () => {
    spyOn(component, 'ngOnInit').and.callThrough();
    component.ngOnInit();
    expect(component.returnUrl).toEqual('/list-book');
  });

  it('should call login', () => {
      spyOn(authService, 'login');
      component.loginForm.controls.email.setValue('admin@gmail.com');
      component.loginForm.controls.password.setValue('123123');
      const button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      expect(authService.login).toHaveBeenCalled();
    });

  it('should call login return success', () => {
    const response = {
      result: 201,
      data: {
        role: 'admin',
        token: 'sdqwexcoash'
      }
    };
    spyOn(authService, 'login').and.returnValue(of(response));

    component.loginForm.controls.email.setValue('admin@gmail.com');
    component.loginForm.controls.password.setValue('123123');
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    expect(authService.isLoggedIn).toEqual(true);
    expect(authService.currentUser).toEqual('admin');
  });
});

// describe('shallow  test LoginComponent', () => {
//
// }
